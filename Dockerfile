FROM tensorflow/tensorflow:1.15.5-gpu-py3

RUN --mount=type=cache,target=/root/.cache/pip \
    pip install -U \
    -i https://pypi.tuna.tsinghua.edu.cn/simple \
    tensorflow_probability==0.7 \
   	imageio \
    scikit-image

WORKDIR /workspace
VOLUME /workspace
